const express = require('express')
const app = express()
const playerData = require('./shipment.json')

const port = process.env.PORT || 3000

app.get('/', (req, res) => {
    res.send("shipment")
})

app.get('/orders', (req, res) => {
    res.send(playerData)
})

app.listen(port, () => {
    console.log(`App is listening to port ${port}`)
})
